python-waiting (1.5.0-1) unstable; urgency=medium

  * New upstream release
  * Update build system to pyproject and hatchling.
  * Update Standards-Version to 4.7.0 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Wed, 11 Sep 2024 02:14:19 +1000

python-waiting (1.4.1-7) unstable; urgency=medium

  * Minor packaging updates from lintian-brush.

 -- Stuart Prescott <stuart@debian.org>  Mon, 30 Jan 2023 16:56:35 +1100

python-waiting (1.4.1-6) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Stuart Prescott ]
  * Update Standards-Version to 4.5.1 (no changes required).
  * Update to debhelper-compat (= 13).
  * Add Rules-Requires-Root: no.
  * Update watch file format to 4.

 -- Stuart Prescott <stuart@debian.org>  Mon, 21 Dec 2020 13:14:05 +1100

python-waiting (1.4.1-5) unstable; urgency=medium

  * Only test with py3versions -s in autopkgtests.
  * Update Standards-Version to 4.5.0 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Tue, 24 Mar 2020 20:41:25 +1100

python-waiting (1.4.1-4) unstable; urgency=medium

  * Drop Python 2 package python-waiting.
  * Update to debhelper 12.
  * Update Standards-Version to 4.4.0 (no changes required).

 -- Stuart Prescott <stuart@debian.org>  Thu, 08 Aug 2019 00:47:43 +1000

python-waiting (1.4.1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/copyright: Use https protocol in Format field

  [ Stuart Prescott ]
  * Update Standards-Version to 4.3.0 (no changes required).
  * Use debhelper-compat (= 11)

 -- Stuart Prescott <stuart@debian.org>  Sun, 27 Jan 2019 00:19:32 +1100

python-waiting (1.4.1-2) unstable; urgency=medium

  * Add python3-all to dependencies for autopkgtest tests.

 -- Stuart Prescott <stuart@debian.org>  Tue, 24 Oct 2017 23:26:16 +1100

python-waiting (1.4.1-1) unstable; urgency=medium

  * New upstream release.
  * Update Standards-Version to 4.0.0 (no changes required)

 -- Stuart Prescott <stuart@debian.org>  Tue, 27 Jun 2017 18:15:19 +1000

python-waiting (1.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Add autopkgtest tests.
  * Add package to collab-maint.

 -- Stuart Prescott <stuart@debian.org>  Fri, 18 Nov 2016 02:06:03 +1100

python-waiting (1.3.0-1) unstable; urgency=low

  * Initial packaging.

 -- Stuart Prescott <stuart@debian.org>  Wed, 10 Feb 2016 21:34:21 +1100
